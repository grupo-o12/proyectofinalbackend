package mision.tic.BackendProyectoO12.repositorio;

import mision.tic.BackendProyectoO12.modelo.Genero;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GeneroRepositorio extends JpaRepository<Genero,Integer> {
}
