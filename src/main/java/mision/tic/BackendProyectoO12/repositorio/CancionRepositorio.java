package mision.tic.BackendProyectoO12.repositorio;

import mision.tic.BackendProyectoO12.modelo.Cancion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CancionRepositorio extends JpaRepository<Cancion,Integer> {
}
