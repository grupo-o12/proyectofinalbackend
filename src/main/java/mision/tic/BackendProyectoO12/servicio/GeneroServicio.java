package mision.tic.BackendProyectoO12.servicio;

import mision.tic.BackendProyectoO12.modelo.Genero;
import mision.tic.BackendProyectoO12.repositorio.GeneroRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GeneroServicio {
    @Autowired
    GeneroRepositorio generoRepositorio;

    public List<Genero> obtenergeneros(){
        return generoRepositorio.findAll();
    }

    public Optional<Genero> obtenergenerosid(int id){
        return generoRepositorio.findById(id);
    }

}
