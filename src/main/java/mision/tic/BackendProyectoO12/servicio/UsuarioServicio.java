package mision.tic.BackendProyectoO12.servicio;

import mision.tic.BackendProyectoO12.modelo.Usuario;
import mision.tic.BackendProyectoO12.repositorio.UsuarioRepositorio;
import mision.tic.BackendProyectoO12.salidas.LoginSalida;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServicio {

    @Autowired
    UsuarioRepositorio usuarioRepositorio;


    public LoginSalida iniciarsesion(String correo, String contraseña) {
        if(usuarioRepositorio.existsByCorreo(correo)){
            Usuario usuario = usuarioRepositorio.findByCorreo(correo).get();
            if(contraseña.equals(usuario.getContraseña())){
                usuario.setEstado(true);
                usuarioRepositorio.save(usuario);
                LoginSalida loginSalida = new LoginSalida(usuario.getId(),true);
                return loginSalida;
            }else {
                LoginSalida loginSalida = new LoginSalida(0,false);
                return loginSalida;
            }
        }else {
            LoginSalida loginSalida = new LoginSalida(0,false);
            return loginSalida;
        }
    }

    public boolean crearusuario(Usuario usuario) {
        try {
            usuario.setEstado(false);
            usuarioRepositorio.save(usuario);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public Usuario obtenerUsuario(int id) {
        try {
            return usuarioRepositorio.findById(id).get();
        }catch (Exception e){
            return null;
        }
    }
}
