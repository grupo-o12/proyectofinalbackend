package mision.tic.BackendProyectoO12.servicio;

import mision.tic.BackendProyectoO12.modelo.Cancion;
import mision.tic.BackendProyectoO12.repositorio.CancionRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CancionesServicio {

    @Autowired
    CancionRepositorio cancionRepositorio;


    public List<Cancion> obtenercanciones() {
        return cancionRepositorio.findAll();
    }
}
