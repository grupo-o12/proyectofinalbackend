package mision.tic.BackendProyectoO12;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendProyectoO12Application {

	public static void main(String[] args) {
		SpringApplication.run(BackendProyectoO12Application.class, args);
	}

}
