package mision.tic.BackendProyectoO12.salidas;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginSalida {
    private int id;
    private boolean rta;
}
