package mision.tic.BackendProyectoO12.controlador;

import mision.tic.BackendProyectoO12.modelo.Usuario;
import mision.tic.BackendProyectoO12.salidas.LoginSalida;
import mision.tic.BackendProyectoO12.servicio.UsuarioServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class UsuarioControlador {

    @Autowired
    UsuarioServicio usuarioServicio;

    @GetMapping("/iniciarsesion")
    public LoginSalida iniciarsesion(@RequestParam String correo, @RequestParam String contraseña){
        return usuarioServicio.iniciarsesion(correo,contraseña);
    }

    @PostMapping("/crearactualizarusuario")
    public boolean crearusuario(@RequestBody Usuario usuario){
        return usuarioServicio.crearusuario(usuario);
    }

    @GetMapping("/obtenerUsuario")
    public Usuario obtenerUsuario(@RequestParam int id){
        return usuarioServicio.obtenerUsuario(id);
    }


}
