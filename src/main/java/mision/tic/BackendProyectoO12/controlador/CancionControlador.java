package mision.tic.BackendProyectoO12.controlador;

import mision.tic.BackendProyectoO12.modelo.Cancion;
import mision.tic.BackendProyectoO12.servicio.CancionesServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class CancionControlador {
    @Autowired
    CancionesServicio cancionesServicio;

    @GetMapping("/obtenercanciones")
    public List<Cancion> obtenercanciones(){
        return cancionesServicio.obtenercanciones();
    }
}
