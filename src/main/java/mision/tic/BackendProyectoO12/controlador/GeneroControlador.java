package mision.tic.BackendProyectoO12.controlador;

import mision.tic.BackendProyectoO12.modelo.Genero;
import mision.tic.BackendProyectoO12.servicio.GeneroServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class GeneroControlador {

    @Autowired
    GeneroServicio generoServicio;

    @GetMapping("/obtenergeneros")
    public List<Genero> obtenergeneros(){
        return generoServicio.obtenergeneros();
    }

    @GetMapping("/obtenergenerosid")
    public Optional<Genero> obtenergenerosid(@RequestParam int id){
        return generoServicio.obtenergenerosid(id);
    }

}
